# 1) meta-tns-stm
This meta is made to be used by [Yocto Project](https://www.yoctoproject.org/). It is made to build images for the stm32mp157F-DK2 board.

### manifest
The manifest can be found [here](https://gitlab.com/e-baret/manifest). This manifest contains all the dependencies to build an image for stm32mp1.

### firmwares
The different firmwares used on this layer can be found [here](https://gitlab.com/e-baret/stm32mp1-fw). In this repository you can found codes of firmwares.


### How to flash image from .raw file

Once yocto build's is over, you can find your image in "< workspace >/build/tmp/deploy/images/< your image >"

To create an image, change your current directory to "< your image >/scripts" and launch the following command :
~~~
./scripts/create_sdcard_from_flashlayout.sh ./flashlayout_<your image>/trusted/FlashLayout_sdcard_stm32mp157f-dk2-trusted.tsv 
~~~
Your image will be created with the following name in the current directory : 
    FlashLayout_sdcard_stm32mp157f-dk2-trusted.raw

Or you can also use the script build-image.sh locate at the root of this layer.
~~~
    ./build-image.sh tns-image-dev
~~~

For flash SD card you can use [balenaEtcher](https://www.balena.io/etcher/), once is installed, select your image, your drive and flash.

### How to flash image from .wks file

First you need to chose a wks file in [wic] directory, and add the followings lines in your image.bb file :
~~~
    WKS_IMAGE_FSTYPES += "wic wic.bz2 wic.bmap"
    WKS_FILE += "sdcard-stm32mp157f-dk2-trusted-dualroot.wks.in"
~~~

Next you just need to build your image, and get the files < your image >.wic.bz and < your image >.wic.bmap in deploy directory.
To finish you need to flash your drive with the wollowing command :
~~~
    sudo bmaptool copy --bmap < your image >.wic.bmap < your image >.wic.bz <driver path>
~~~

# 2) recipes-bsp
This recipe modifies stm32 uboot layer. 
The firmware thats starts during boot is located in the directory "recipes-bsp/u-boot/u-boot-stm32mp-extlinux". 

NB : The name of the binary must be "rproc-ma-fw.elf".

# 3) recipes-core
This recipe just gives a static IP to the stm32mp1.

# 4) recipes-graphics
This recipe contains parameters for Weston (screen orientation, background, keyboard layout, launchers, ...).

# 5) recipes-kernel
This recipe contains :
- Devicetree patches
- Kernel configuration files
- Rpmsg-sdb driver (this driver is used to exchange data between A7 and M4 over RAM)
- Packages for trace-cmd and kernelshark

# 6) recipes-tns

This recipe contains TnS images for STM32MP1, it's also contains many projects to do bandwidth test.

## 6.1) images

### 6.1.a) tns-image-minimal
This image based on the "core-image-minimal" image from poky. It's contains minimal tools for SSH. 
It's also removed unused features for the STM32MP157F-DK2 board.

### 6.1.b) tns-image-weston
This image is based on "tns-image-minimal". It's adding a graphical interface from Weston and X11 server. The graphical interface contains a terminal and a text editor.

### 6.1.c) tns-image-dev

This image is based on "tns-image-weston". It's contains the following things  :  
- Tools for package management
- Build tools for c/c++ and cmake
- Requirements to use remote ssh from VSCode
- Packages from recipes-tns, firmwares for M4, rpmsg driver
- Packages & Drivers for Wi-Fi, Bluetooth and CAN

## 6.2) bandwidth-tty

This layer it's used to make bandwidth test over virtuart. This layer need firmware [fw-bandwidth-tty](https://gitlab.com/e-baret/stm32mp1-fw/-/tree/main/fw-bandwidth-tty) to work. The binary can be found in the directory [recipes-tns/bandwidth-tty/bandwidth-tty/firmware](https://gitlab.com/e-baret/meta-tns-stm/-/tree/dunfell/recipes-tns/bandwidth-tty/bandwidth-tty/firmware).

### 6.2.a) How it's work

This diagram show you how to initialise VirtUart and used it. In this case VirtUart 0 it's used, but you can do the same with VirtUart 1.

~~~plantuml
@startuml
participant A7 order 10
participant M4 order 30

==Initialization==
M4->M4: MX-OPENAMP_Init(...)
note left
MX-OPENAMP_Init() waiting for rpmsg
initialization to initialise OpenAMP.
If A7 is already up, rpmsg is init and this 
function doesn't wait 
end note
M4->M4: VIRT_UART_Init(&hvirtuart0)
M4->M4: VIRT_UART_RegisterCallback(&hvirtuart0, \n        VIRT_UART_RXCPLT_CB_ID,\n        VIRT_UART0_RxCpltCallback)

A7->A7: port0 = open("/dev/ttyRPMSG0", O_RDWR);
A7->A7: init_tty(&port0)

==Send data from A7 to M4==
A7->M4: write(*port0, data, data length);
M4->M4: VIRT_UART0_RxCpltCallback()
M4->M4: User_VIRT_UART0_Treatment()

==Send data from M4 to A7==
A7<-M4: VIRT_UART_Transmit(&hvirtuart0, data, data length);
A7->A7:read(*port0, buffer, buffer size); 
@enduml
~~~

### 6.2.b) Bandwidth test over VirtUart
The M4 firmware and A7 program run several bandwidth tests. To make test two VirtUart are created. Each test follows this operation : 

~~~plantuml
@startuml
participant A7 order 10
participant M4 order 30

A7->A7: starts the timer 
A7->M4: ttyRPMSG0 : sends data
A7<-M4: ttyRPMSG1 : ACK
A7->A7: stops the timer 

A7->A7: starts the timer
A7->M4: ttyRPMSG1 : ACK
A7<-M4: ttyRPMSG0 : sends back data 
A7->A7: stops the timer  

A7-> A7: bandwidth calculation
@enduml
~~~

By adding this layer, C program is installed in "/home/root/dev/bandwidth-tty", and the firmware binary's is installed in /lib/firmware directory.

### 6.2.c) Stress test effect on bandwidth

To make stress test you should use the function make_stress_test(). This function can be found in file [stress-serial.c](https://gitlab.com/e-baret/meta-tns-stm/-/blob/dunfell/recipes-tns/bandwidth-tty/bandwidth-tty/src/stress-serial.c).

This function need the package stress-ng to work.

Make_stress_test() will make severals bandwdith test, and create child to stress the board with stress-ng.

## 6.3) bandwidth-ddr

This layer it's used to make bandwidth test over DDR (ram). This layer need the firmware [fw-bandwidth-ddr](https://gitlab.com/e-baret/stm32mp1-fw/-/tree/main/fw-bandwidth-ddr) to work. The binary can be found in the directory [recipes-tns/bandwidth-ddr/bandwidth-ddr/firmware](https://gitlab.com/e-baret/meta-tns-stm/-/tree/dunfell/recipes-tns/bandwidth-ddr/bandwidth-ddr/firmware).

This layer need the driver [rpmsg_sdb_driver](https://gitlab.com/e-baret/meta-tns-stm/-/tree/dunfell/recipes-kernel/rpmsg-sdb-mod/rpmsg-sdb-mod), this driver is in "recipes-kernel/rpmsg-sdb-mod". You can install it with the following line : 

~~~
CORE_IMAGE_EXTRA_INSTALL += "rpmsg-sdb-mod"
~~~

By adding this layer, C program is installed in "/home/root/dev/bandwidth-ddr", and the firmware binary's is installed in /lib/firmware directory.

### 6.3.a) Init SDB (Shared Data Buffer)
To initialise SDB, this program make followings exchanges with sdb driver and M4 :

~~~plantuml
@startuml
participant A7 order 10
participant rpmsg_sdb_driver order 20
participant M4 order 30

A7->rpmsg_sdb_driver : fsdb=open("/dev/rpmsg_sdb")

A7->M4: ttyRPMSG1: "Bn"
note right
n=number of shared data buffer to init
The max value of n is 10 
end note
A7->rpmsg_sdb_driver: buff_0=mmap(size,fsdb)
rpmsg_sdb_driver->M4: B0AxxxxxxxxLyyyyyyyy
note right
xxxxxxxx is the physical address of the buffer in the DDR 
Format : 32 bits, 8-digit hexadecimal
end note

A7->rpmsg_sdb_driver: buff_1=mmap(size,fsdb)
rpmsg_sdb_driver->M4: B1AxxxxxxxxLyyyyyyyy
note right
yyyyyyyy is the length of the buffer 
Format : 32 bits, 8-digit hexadecimal
end note

A7->rpmsg_sdb_driver: ...
rpmsg_sdb_driver->M4: ...

A7->rpmsg_sdb_driver: buff_n=mmap(size,fsdb)
rpmsg_sdb_driver->M4: BnAxxxxxxxxLyyyyyyyy
@enduml
~~~

### 6.3.b) Exchange data from M4 to A7
To read data from A7, and write with M4 this program make followings exchanges : 

~~~plantuml
@startuml
participant A7 order 10
participant rpmsg_sdb_driver order 20
participant M4 order 30

M4->M4: memcpy(addrBuff_0, data, length)
M4->rpmsg_sdb_driver: B0Lyyyyyyyy
A7<-rpmsg_sdb_driver: B0Lyyyyyyyy
A7->A7: memcpy(data, addrBuff_0, length)

M4->M4: memcpy(addrBuff_1, data, length)
M4->rpmsg_sdb_driver: B1Lyyyyyyyy
A7<-rpmsg_sdb_driver: B1Lyyyyyyyy
A7->A7: memcpy(data, addrBuff_1, length)

M4->rpmsg_sdb_driver: ...
A7<-rpmsg_sdb_driver: ...

M4->M4: memcpy(addrBuff_n, data, length)
M4->rpmsg_sdb_driver: BnLyyyyyyyy
A7<-rpmsg_sdb_driver: BnLyyyyyyyy
A7->A7: memcpy(data, addrBuff_n, length)
@enduml
~~~
In this diagram : 
- 'n' is the id of shared data buffer
- 'yyyyyyyy' is the length of the buffer (32 bits, 8-digit hexadecimal format) 


### 6.3.c) Exchange data from A7 to M4
To write data from A7, and read with M4 this program make followings exchanges :

~~~plantuml
@startuml
participant A7 order 10
participant rpmsg_sdb_driver order 20
participant M4 order 30

A7->A7: memcpy(addrBuff_0, data, length)
A7->rpmsg_sdb_driver: B0Lyyyyyyyy
rpmsg_sdb_driver->M4: B0Lyyyyyyyy
M4->M4: memcpy(data, addrBuff_0, length)


A7->A7: memcpy(addrBuff_1, data, length)
A7->rpmsg_sdb_driver: B0Lyyyyyyyy
rpmsg_sdb_driver->M4: B0Lyyyyyyyy
M4->M4: memcpy(data, addrBuff_1, length)

A7->rpmsg_sdb_driver: ...
rpmsg_sdb_driver->M4: ...

A7->A7: memcpy(addrBuff_n, data, length)
A7->rpmsg_sdb_driver: B0Lyyyyyyyy
rpmsg_sdb_driver->M4: B0Lyyyyyyyy
M4->M4: memcpy(data, addrBuff_n, length)
@enduml
~~~

In this diagram : 
- 'n' is the id of shared data buffer
- 'yyyyyyyy' is the length of the buffer (32 bits, 8-digit hexadecimal format) 

### 6.3.d) Bandwidth test over memory

The M4 firmware and A7 program run several bandwidth tests, to make this test 2 SDB are needed. Each test follows this operation :

~~~plantuml
@startuml

participant A7 order 10
participant rpmsg_sdb_driver order 20
participant M4 order 30
!pragma teoz true

A7-->A7: clock_gettime(..., &t_start)

A7->A7:memcpy(addrBuff_0, data, length)

A7->rpmsg_sdb_driver: B0lyyyyyyyy
A7-->A7: clock_gettime(..., &t_A7write)
&rpmsg_sdb_driver->M4: B0Lyyyyyyyy
M4->M4:memcpy(addrBuff_1, addrBuff_0, length)
M4->rpmsg_sdb_driver: B1Lyyyyyyyy
A7<-rpmsg_sdb_driver: B1Lyyyyyyyy
A7-->A7: clock_gettime(..., &t_M4memcpy)

A7->A7:memcpy(tmpbuff, addrBuff_0, length)
A7-->A7: clock_gettime(..., &t_A7read)

A7->A7: bandwidth calculation

@enduml
~~~
In this diagram 'yyyyyyyy' is the length of the buffer (32 bits, 8-digit hexadecimal format) 

### 6.3.e) Stress test effect on bandwidth

To make stress test you should use the function SDB_stress_test(). This function can be found in file [SDB_rpmsg.c](https://gitlab.com/e-baret/meta-tns-stm/-/blob/dunfell/recipes-tns/bandwidth-ddr/bandwidth-ddr/src/SDB_rpmsg.c).

This function need the package stress-ng to work.

SDB_stress_test() will make severals bandwdith test, and create child to stress the board with stress-ng.
