Up & Down device:
~~~shell
ifconfig wlan0 up
ifconfig wlan0 down
~~~

Scan Wi-Fi
~~~shell
iw dev wlan0 scan | grep SSID:
~~~

Show the wifi the card is connected to
~~~shell
iw dev wlan0 info
~~~

Change WLAN IP
~~~shell
ifconfig wlan0 XXX.XXX.XXX.XXX
~~~

Set password to Wi-Fi:
~~~shell
wpa_passphrase "<SSID>" "<PWD>" >> /etc/wpa_supplicant.conf
~~~

Connect to Wi-Fi : 
~~~shell
wpa_supplicant -Dnl80211 -iwlan0 -c /etc/wpa_supplicant.conf
~~~

