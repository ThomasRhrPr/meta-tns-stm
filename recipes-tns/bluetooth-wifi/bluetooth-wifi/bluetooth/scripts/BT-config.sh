#!/bin/sh

DEVICE="hci0"
BTNAME="BT-STM32MP1"
MACADR="0xAA 0xBB 0xCC 0xDD 0xEE 0xFF"

hciconfig $DEVICE up
hciconfig $DEVICE name $BTNAME
hcitool cmd 0x3F 0x001 $MACADR >/dev/null
hciconfig $DEVICE down

hciconfig $DEVICE up piscan
hciconfig $DEVICE
