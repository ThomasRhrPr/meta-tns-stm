~~~plantuml
@startuml
participant A7 order 10
participant rpmsg_sdb_driver order 20
participant M4 order 30

==Initialization==
A7->rpmsg_sdb_driver : fsdb=open("/dev/rpmsg_sdb")

A7->M4: ttyRPMSG1: "Bn"
A7->rpmsg_sdb_driver: buff_0=mmap(size,fsdb)
rpmsg_sdb_driver->M4: B0AxxxxxxxxLyyyyyyyy
...
A7->rpmsg_sdb_driver: buff_n=mmap(size,fsdb)
rpmsg_sdb_driver->M4: BnAxxxxxxxxLyyyyyyyy


==Send data from A7 to M4==
A7->A7: memcpy(addrBuff_0, data, length)
A7->rpmsg_sdb_driver: B0Lyyyyyyyy
rpmsg_sdb_driver->M4: B0Lyyyyyyyy
M4->M4: memcpy(data, addrBuff_0, length)
==Send data from M4 to A7==
M4->M4: memcpy(addrBuff_0, data, length)
M4->rpmsg_sdb_driver: B0Lyyyyyyyy
A7<-rpmsg_sdb_driver: B0Lyyyyyyyy
A7->A7: memcpy(data, addrBuff_0, length)

note over A7, M4
- n is the number of shared data buffer to initialize. The max value of n is 10 

- xxxxxxxx is the physical address of the buffer in the DDR. Format : 32 bits, 8-digit hexadecimal

- yyyyyyyy is the length of the buffer. Format : 32 bits, 8-digit hexadecimal
end note

==Free ressources== 
A7->A7: munmap(buff_0, buffer size);
@enduml
~~~