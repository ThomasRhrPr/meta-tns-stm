#include "SDB_rpmsg.h"

static int efd[NB_BUF];
static struct pollfd fds[NB_BUF];
void* mmappedData[NB_BUF];

char *driver = "/dev/rpmsg-sdb";

rpmsg_sdb_ioctl_set_efd q_set_efd;
rpmsg_sdb_ioctl_get_data_size q_get_data_size;
rpmsg_sdb_ioctl_send_data q_send_data;


// The file descriptor used to manage our SDB over RPMSG 
static int fd_SDB_rpmsg = -1;


int8_t SDB_Init(void){
    
	fd_SDB_rpmsg = open(driver, O_RDWR);
    assert(fd_SDB_rpmsg != -1);

    for (int i=0;i<NB_BUF;i++){
        // Create the evenfd, and sent it to kernel driver, for notification of buffer full
        efd[i] = eventfd(0, 0);
        if (efd[i] == -1){
            error(EXIT_FAILURE, errno, "failed to get eventfd");
            return -1;
        }
        printf("\nForward efd info for buf%d with fd_SDB_rpmsg:%d and efd:%d\n",i,fd_SDB_rpmsg,efd[i]);
        q_set_efd.bufferId = i;
        q_set_efd.eventfd = efd[i];
        if(ioctl(fd_SDB_rpmsg, RPMSG_SDB_IOCTL_SET_EFD, &q_set_efd) < 0){
            error(EXIT_FAILURE, errno, "failed to set efd");
             return -1;
        }
        // watch eventfd for input
        fds[i].fd = efd[i];
        fds[i].events = POLLIN;
        //mmappedData[i] = mmap(NULL, DATA_BUF_POOL_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd_SDB_rpmsg, 0);
        mmappedData[i] = mmap(NULL, DATA_BUF_POOL_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd_SDB_rpmsg, 0);
        printf("\nmmappedData[%d]:%p\n", i, mmappedData[i]);
        assert(mmappedData[i] != MAP_FAILED);
        usleep(50000); //50ms
    }
    return 0;
}

int8_t SDB_Write(uint8_t bufferId, char* bufferWrite, uint32_t size){
      
    unsigned char* pData = (unsigned char*)mmappedData[bufferId]; 

    if(bufferId >= NB_BUF){
        printf("error(%s) : %d it's not a valid buffer id (from 0 to %d)\n", __func__, bufferId, (NB_BUF-1));
        return -1;
    }

    if(size <= DATA_BUF_POOL_SIZE){
        memcpy((unsigned char*)pData, bufferWrite, size);
    }else{
        return -2; 
    }

    q_send_data.bufferId = bufferId;
    q_send_data.size = size;

    //printf("ioctl RPMSG_SDB_IOCTL_SEND_DATA : buffId=%d size=%d\n", q_send_data.bufferId, q_send_data.size);
    ioctl(fd_SDB_rpmsg, RPMSG_SDB_IOCTL_SEND_DATA, &q_send_data);

    return 0;
}

int8_t SDB_Read(uint8_t bufferId, char* bufferRead, uint32_t size, uint16_t timeout_ms){
    int ret, rc;
    char buf[16];

    if(bufferId >= NB_BUF){
        printf("error(%s) : %d it's not a valid buffer id (from 0 to %d)\n", __func__, bufferId, (NB_BUF-1));
        return -1;
    }

    // wait till at least one buffer becomes available
    ret = poll(fds, NB_BUF, timeout_ms);
    
    if (ret == -1){
        perror("poll()"); return -1;
    }else if (ret == 0){
        printf("%s : No buffer data within %dms.\n", __func__, timeout_ms );
        return 1;
    }else{ // buffer is available 
        if (fds[bufferId].revents & POLLIN) {
            rc = read(efd[bufferId], buf, 16);
            if (!rc) {
                printf("%s : stdin closed\n", __func__);
                return 0;
            }
            // Get buffer data size
            q_get_data_size.bufferId = bufferId;
    
            if(ioctl(fd_SDB_rpmsg, RPMSG_SDB_IOCTL_GET_DATA_SIZE, &q_get_data_size) < 0) {
                error(EXIT_FAILURE, errno, "Failed to get data size");
            }
    
            if (q_get_data_size.size) {
                unsigned char* pData = (unsigned char*)mmappedData[bufferId];
                memcpy(bufferRead, pData, q_get_data_size.size);
            }else{
                printf("%s : buf[%d] is empty\n", __func__, bufferId);
            }
        }
    }

    return 0;
}

void SDB_Echo(int SDBread,int SDBwrite, char* data, int datasize, int timeout){

    char bufferEchoW[datasize], bufferEchoR[datasize];

    memset(bufferEchoW, '\0', sizeof(bufferEchoW));
    memset(bufferEchoR, '\0', sizeof(bufferEchoR));
            
    // ECHO WRITE 
    //sprintf(bufferEchoW,"echo from A7 to M4 - %d", i);
    sprintf(bufferEchoW,"%s", data);

    SDB_Write(SDBwrite, bufferEchoW, datasize);
    
    printf("%s(w) : buffId=%d size=%d data=%s\n", __func__, SDBwrite, datasize, bufferEchoW);
                    
    // ECHO READ
    if(!SDB_Read(SDBread, bufferEchoR, datasize, timeout) ){
        printf("%s(r) : buffId=%d size=%u data=%s\n",__func__, SDBread,  datasize, bufferEchoR);
    }
}

void SDB_Clear(uint8_t bufferId, uint32_t size){
    unsigned char* pData = (unsigned char*)mmappedData[bufferId]; 
    memset(pData, 0, size);
}

int8_t SDB_Write_bigdata(uint8_t bufferId, char* bufferWrite, uint32_t buffersize, uint32_t block_size){
      
    uint32_t offset = 0;  
    unsigned char* pData = (unsigned char*)mmappedData[bufferId]; 

    if(bufferId >= NB_BUF){
        printf("error(%s) : %d it's not a valid buffer id (from 0 to %d)\n", __func__, bufferId, (NB_BUF-1));
        return -1;
    }

    if(block_size <= DATA_BUF_POOL_SIZE ){
        for(offset=0; offset<block_size; offset+=buffersize){
            memcpy( (((unsigned char*)pData)+offset), bufferWrite, buffersize);
             //printf("ptr : %p, %x\n", (((unsigned char*)pData)+offset), (((unsigned char*)pData)+offset));
        }
    }else{
        return -2;
    }

    q_send_data.bufferId = bufferId;
    q_send_data.size = block_size;

    ioctl(fd_SDB_rpmsg, RPMSG_SDB_IOCTL_SEND_DATA, &q_send_data);

    return 0;
}

int32_t SDB_WaitToRead(uint8_t bufferId, uint16_t timeout_ms){
    int ret, rc;
    char buf[16];

    if(bufferId >= NB_BUF){
        printf("error(%s) : %d it's not a valid buffer id (from 0 to %d)\n", __func__, bufferId, (NB_BUF-1));
        return -1;
    }

    // wait till at least one buffer becomes available
    ret = poll(fds, NB_BUF, timeout_ms);
    
    if (ret == -1){
        perror("poll()"); 
        return -1;
    }else if (ret == 0){
        printf("%s : No buffer data within %dms.\n", __func__, timeout_ms );
        return -2;
    }else{ // buffer is available 
        if (fds[bufferId].revents & POLLIN) {
            rc = read(efd[bufferId], buf, 16);
            if (!rc) {
                printf("%s : stdin closed\n", __func__);
                return -3;
            }
            // Get buffer data size
            q_get_data_size.bufferId = bufferId;
    
            if(ioctl(fd_SDB_rpmsg, RPMSG_SDB_IOCTL_GET_DATA_SIZE, &q_get_data_size) < 0) {
                error(EXIT_FAILURE, errno, "Failed to get data size");
            }
    
            if (q_get_data_size.size) {
 
                return q_get_data_size.size;
            }else{
                printf("%s : buf[%d] is empty\n", __func__, bufferId);
                return 0;
            }
        }
    }

    return 0;
}

void SDB_DeInit(void){
    for (int i=0;i<NB_BUF;i++){
		munmap(mmappedData[i], DATA_BUF_POOL_SIZE);
	}
    close(fd_SDB_rpmsg);
}
    
void SDB_bandwidth_test(int SDBread,int SDBwrite,struct test *test_list, uint16_t nb_tests, uint32_t block_size, uint8_t debug){
	static struct timespec tglobal_start, tglobal_stop;
    static struct timespec t_1, t_2;
    
    unsigned char* pDataRead = (unsigned char*)mmappedData[SDBread]; 
    unsigned char* pDataWrite = (unsigned char*)mmappedData[SDBwrite]; 

    char databuff[1048576]={'\0'}; // 1024*1024 = 1048576 = 1Mo
    char bwbuff[1048576]={'\0'}; // 1024*1024 = 1048576 = 1Mo
	init_buffer((char *)databuff, block_size);

    for(int i=0;i<nb_tests; i++){

        // A7 write
        clock_gettime(CLOCK_REALTIME, &tglobal_start);
        SDB_Write(SDBwrite, databuff, block_size);
        clock_gettime(CLOCK_REALTIME, &t_1);

        // wait for M4
        test_list[i].bytesread = SDB_WaitToRead(SDBread, 10000);
        if(test_list[i].bytesread < 0){test_list[i].bytesread=0;}
        
        //A7 read
        clock_gettime(CLOCK_REALTIME, &t_2);
        memcpy(bwbuff, pDataRead, block_size);
        clock_gettime(CLOCK_REALTIME, &tglobal_stop);
        
        test_list[i].globaltime  = timespec_diff_ns (tglobal_start, tglobal_stop);
        test_list[i].A7writetime  = timespec_diff_ns (tglobal_start, t_1);
        test_list[i].M4readwritetime  = timespec_diff_ns (t_1, t_2);
        test_list[i].A7readtime  = timespec_diff_ns (t_2, tglobal_stop);

        test_list[i].id = i;
        test_list[i].size = block_size;
        //test_list[i].state = strcmp(pDataWrite, pDataRead);
        test_list[i].state = memcmp(pDataWrite, pDataRead, block_size);
        
        SDB_Clear(SDBread, block_size); SDB_Clear(SDBwrite, block_size); 
        if(debug){
            printf("-> test: id=%d, blocksize=%do, bytesread=%d, time(rw)=%.04fus, state=%d \n", \
                    test_list[i].id, test_list[i].size, test_list[i].bytesread, \
                    test_list[i].globaltime/1000.0,  \
                    test_list[i].state); 
        }
    }

}


// Init data buffer to send during test 
void init_buffer(char *buffer, uint16_t buffer_size){
    time_t t;
    srand((unsigned) time(&t)); //Intializes random number generator
    for(int i=0; i<buffer_size; i++){
        buffer[i]=33+(rand()%94); // ASCII from "!"(33) to "~"(126) for avoid "void" and control char
    }
   //printf("%s: %s", __func__, buffer);
}

// Calc and print bandwidth of tests 
void print_result(struct test *test_list, int nb_tests, int block_size){

   
    float tmoy_global=0, tmoy_A7w=0, tmoy_A7r=0, tmoy_M4rw=0;
    float bandwidth_global=0, bandwidth_A7w=0, bandwidth_A7r=0, bandwidth_M4rw=0;
    int ValidTest=0;

    for(int i=0; i<nb_tests; i++){
         if( (test_list[i].state == 0) && (test_list[i].bytesread==test_list[i].size)){
            tmoy_global+=test_list[i].globaltime;
            tmoy_A7w+=test_list[i].A7writetime;
            tmoy_A7r+=test_list[i].A7readtime;
            tmoy_M4rw+=test_list[i].M4readwritetime;
            ValidTest++;
        }
    }

    if(ValidTest){
        tmoy_global/=ValidTest;
        tmoy_A7w/=ValidTest;
        tmoy_A7r/=ValidTest;
        tmoy_M4rw/=ValidTest;

        bandwidth_global= ((float)block_size*2) / tmoy_global; 
        bandwidth_A7w= block_size / tmoy_A7w;
        bandwidth_A7r= block_size / tmoy_A7r;
        bandwidth_M4rw= block_size*2 / tmoy_M4rw;
        
    }

    printf("\nBlocks size: %d \
            \nValid tests: %d/%d - %.02f%% \
            \n          Average time(us)  Bandwidth(ko/s) \
            \n-global   %12.04f %17.04f \
            \n-A7w      %12.04f %17.04f \
            \n-A7r      %12.04f %17.04f \
            \n-M4rw     %12.04f %17.04f \
            \n", \
            block_size, ValidTest, nb_tests, ((float)ValidTest/(float)nb_tests)*100.0, \
            tmoy_global/1000, bandwidth_global*1000000,   \
            tmoy_A7w/1000, bandwidth_A7w*1000000, \
            tmoy_A7r/1000, bandwidth_A7r*1000000, \
            tmoy_M4rw/1000, bandwidth_M4rw*1000000 \
        );
}

// Init save file
void init_save_file(char *filename){
    FILE *fp; 
    char buffer[300]={0};

    fp = fopen(filename, "w");
    if ( fp == NULL ) {
        fprintf( stderr, "Cannot open file for writing\n" );
        exit( -1 );
    }

    sprintf(buffer, "Blocks size,Valid tests,Number of tests,Succes(%%),Average time (global)(us),Average time (A7w)(us),Average time (A7r)(us),Average time (M4wr)(us),Bandwidth (global)(ko/s),Bandwidth(A7w)(ko/s),Bandwidth(A7r)(ko/s),Bandwidth(M4rw)(ko/s)\n");
    fputs(buffer, fp);

    fclose(fp);

}

// Save results in the given file (filename)
void save_result(struct test *test_list,char *filename, int nb_tests, int block_size){
    FILE *fp; 
    char buffer[300]={0};

    fp = fopen(filename, "a");
    if ( fp == NULL ) {
        fprintf( stderr, "Cannot open file for writing\n" );
        exit( -1 );
    }

    float tmoy_global=0, tmoy_A7w=0, tmoy_A7r=0, tmoy_M4rw=0;
    float bandwidth_global=0, bandwidth_A7w=0, bandwidth_A7r=0, bandwidth_M4rw=0;
    int ValidTest=0;

    for(int i=0; i<nb_tests; i++){
         if( (test_list[i].state == 0) && (test_list[i].bytesread==test_list[i].size)){
            tmoy_global+=test_list[i].globaltime;
            tmoy_A7w+=test_list[i].A7writetime;
            tmoy_A7r+=test_list[i].A7readtime;
            tmoy_M4rw+=test_list[i].M4readwritetime;
            ValidTest++;
        }
    }

    if(ValidTest){
        tmoy_global/=ValidTest;
        tmoy_A7w/=ValidTest;
        tmoy_A7r/=ValidTest;
        tmoy_M4rw/=ValidTest;

        bandwidth_global= ((float)block_size*2) / tmoy_global; 
        bandwidth_A7w= block_size / tmoy_A7w;
        bandwidth_A7r= block_size / tmoy_A7r;
        bandwidth_M4rw= block_size*2 / tmoy_M4rw;
        
    }

    sprintf(buffer, "%d,%d,%d,%.04f,%.04f,%.04f,%.04f,%.04f,%.04f,%.04f,%.04f,%.04f\n", \
            block_size, ValidTest, nb_tests, ((float)ValidTest/(float)nb_tests)*100.0, \
            tmoy_global/1000, tmoy_A7w/1000, tmoy_A7r/1000, tmoy_M4rw/1000, \
            bandwidth_global*1000000, bandwidth_A7w*1000000, bandwidth_A7r*1000000, bandwidth_M4rw*1000000);
    fputs(buffer, fp);

    fclose(fp);
}

// Difference between 2 struct timespec
uint32_t timespec_diff_ns (struct timespec t1, struct timespec t2){
    struct timespec diff;
    
    if (t2.tv_nsec-t1.tv_nsec < 0) {
        diff.tv_sec  = t2.tv_sec - t1.tv_sec - 1;
        diff.tv_nsec = t2.tv_nsec - t1.tv_nsec + 1000000000;
    } else {
        diff.tv_sec  = t2.tv_sec - t1.tv_sec;
        diff.tv_nsec = t2.tv_nsec - t1.tv_nsec;
    }
   // printf("\ntimestruct1:%ld:%ld \ntimestruct2:%ld:%ld \ntimestructd:%ld:%ld \n", t1.tv_sec, t1.tv_nsec, t2.tv_sec, t2.tv_nsec, diff.tv_sec, diff.tv_nsec);
    
    return (uint32_t)((diff.tv_sec * 1000000000.0) + diff.tv_nsec);
}


/***********************  Stress test ***********************/
uint16_t isStressed=0;

void handler_SIGCHLD(int sig) { 
    signal(sig, handler_SIGCHLD);
    isStressed=0;
}

/* SDB_stress_test parameters :
- SDBread & SDBwrite : buffers used to make bandwidth test
- filename : name of the save file
- block_size : data size 
- test_duration : duration of the test in seconds 
- time_btw2stress : time elapsed between two stress test 
- stress_duration_sec : duration of stress test in seconds
- test_type : 
    - 0b0001 : cpu stress
    - 0b0010 : hdd stress
    - 0b0100 : memory stress
    - 0b0111 : cpu, hdd and memory stress
    - others : cpu, hdd, memory, mmap, memcpy stress
- worker : number of workers for each tests
*/
   
void SDB_stress_test(int SDBread,int SDBwrite, char *filename,  uint32_t block_size, uint32_t test_duration_sec, uint32_t time_btw2stress_sec, uint32_t stress_duration_sec, uint8_t test_type, uint8_t worker, uint8_t debug){
	static struct timespec tglobal_start, techo_start, techo_stop, tA7_start, tA7_stop, tglobal_stop, runtime;

    runtime.tv_sec=0; runtime.tv_nsec=0;
    isStressed=0;
    int number_stress_test=0;
    float  time_echo, time_A7w, time_A7r, bw_M4rw, bw_A7w, bw_A7r;
    uint32_t state, bytesread, isValid;
    unsigned char* pDataRead = (unsigned char*)mmappedData[SDBread]; 
    unsigned char* pDataWrite = (unsigned char*)mmappedData[SDBwrite]; 

    char databuff[1048576]={'\0'}; // 1024*1024 = 1048576 = 1Mo
	init_buffer((char *)databuff, block_size);
    pid_t pidf;
    FILE *fp; 
    char buffer[300]={0};

    if(time_btw2stress_sec > test_duration_sec){printf("%s(error): time btw2stress > test duration\n", __func__); return;}
    if(stress_duration_sec > test_duration_sec){printf("%s(error): stress duration > test duration\n", __func__);return;}
    if(time_btw2stress_sec < stress_duration_sec){printf("%s(error): time btw2stress < stress duration\n", __func__);return;}

    //int stress_test2do = test_duration_sec/(time_btw2stress_sec + stress_duration_sec);

    // Init save file 
    fp = fopen(filename, "w");
    if ( fp == NULL ) {
        fprintf( stderr, "Cannot open file for writing\n" );
        exit( -1 );
    }
    sprintf(buffer, "time(s),bytes read,isStressed,isValid,A7w(us),A7r(us),M4rw(us),bw_A7w(ko/s),bw_A7r(ko/s),bw_M4rw(ko/s)\n");
    fputs(buffer, fp);
    fclose(fp);

    clock_gettime(CLOCK_REALTIME, &tglobal_start);

    // start test 
    while(test_duration_sec > runtime.tv_sec)
    {
        // A7 write
        clock_gettime(CLOCK_REALTIME, &tA7_start);
        SDB_Write(SDBwrite, databuff, block_size);
        clock_gettime(CLOCK_REALTIME, &techo_start);
        // wait for M4
        bytesread = SDB_WaitToRead(SDBread, 10000);
        if(bytesread < 0){bytesread=0;}
        clock_gettime(CLOCK_REALTIME, &techo_stop);
        //A7 read
        //memcpy(bwbuff, pDataRead, block_size);
        state = memcmp(pDataWrite, pDataRead, block_size);
        clock_gettime(CLOCK_REALTIME, &tA7_stop);

        // runtime calculation
        if (techo_stop.tv_nsec-tglobal_start.tv_nsec < 0) {
            runtime.tv_sec  = techo_stop.tv_sec - tglobal_start.tv_sec - 1;
            runtime.tv_nsec = techo_stop.tv_nsec - tglobal_start.tv_nsec + 1000000000;
        } else {
            runtime.tv_sec  = techo_stop.tv_sec - tglobal_start.tv_sec;
            runtime.tv_nsec = techo_stop.tv_nsec - tglobal_start.tv_nsec;
        }

        time_echo = timespec_diff_ns (techo_start, techo_stop);
        time_A7r = timespec_diff_ns (tA7_start, techo_start);
        time_A7w = timespec_diff_ns (techo_stop, tA7_stop);

        if(state){isValid=0;}else{isValid=1;}

        //state = memcmp(pDataWrite, pDataRead, block_size);
        //SDB_Clear(SDBread, block_size); SDB_Clear(SDBwrite, block_size); 
        if(isValid){
            bw_M4rw= ((float)block_size*2) / time_echo;
            bw_A7r= block_size / time_A7r,
            bw_A7w= block_size / time_A7w;
        }else{
            bw_M4rw=0; bw_A7r=0; bw_A7w=0;
        }
        fp = fopen(filename, "a");
        sprintf(buffer, "%ld.%ld,%d,%d,%d,%.04f,%.04f,%.04f,%.04f,%.04f,%.04f\n", \
            runtime.tv_sec, runtime.tv_nsec, bytesread, isStressed, isValid, \
            time_A7w/1000, time_A7r/1000, time_echo/1000, bw_A7w*1000000, bw_A7r*1000000, bw_M4rw*1000000);
        fputs(buffer, fp);
        fclose(fp);

        if(debug){
            sprintf(buffer, "[id:%d][%ld.%9ld] isStressed=%d, isValid=%d, bwM4rw(ko/s)=%.04f\n", \
                test_type, runtime.tv_sec, runtime.tv_nsec, isStressed, isValid, bw_M4rw*1000000);
            printf("%s",buffer);
        }
       
        // create child to run stress test 
        if( ((runtime.tv_sec) >= (time_btw2stress_sec + (time_btw2stress_sec + stress_duration_sec)*number_stress_test)) && (isStressed==0) ){
            //number_stress_test++;
            switch(pidf=fork()){
                case -1:	//fork error
                    printf("%s: fork error\n", __func__); exit(-1); break;
                case 0:	    // child
                    start_stress(test_type, worker, stress_duration_sec); break; // make stress test 
                default:  // parent
                    signal(SIGCHLD,handler_SIGCHLD);  // listen for end of child
                    number_stress_test++; isStressed=1;
            }    
        }
    }
    kill(pidf, SIGKILL); // kill child
    wait(NULL); // wait for end of child
    clock_gettime(CLOCK_REALTIME, &tglobal_stop);
}


void start_stress(uint8_t test_type, uint8_t worker, uint32_t timeout ){
    char ctimeout[4] = {'\0'};
    char cworker[4] = {'\0'};
    int ret;

    int fd = open("tmp.log", O_WRONLY | O_APPEND | O_CREAT, 0644);
    if ( fd < 0 ) { fprintf( stderr, "Cannot open file for writing\n" ); exit( -1 );}
    
    dup2(fd, STDOUT_FILENO); // redirect output to fd file 
    dup2(fd,STDERR_FILENO); // redirect logs to fd file 
    
    sprintf(ctimeout, "%d", timeout);
    sprintf(cworker, "%d", worker);
    char *args_cpu[] = {"stress-ng", "--cpu", cworker, "--timeout", ctimeout, "--metrics", NULL};
    char *args_hdd[] = {"stress-ng", "--hdd", cworker, "--timeout", ctimeout, "--metrics", NULL};
    char *args_cpu_hdd[] = {"stress-ng", "--cpu",cworker, "--hdd", cworker, "--timeout", ctimeout, "--metrics", NULL};
    char *args_vm[] = {"stress-ng", "--vm", cworker, "--timeout", ctimeout, "--metrics", NULL};
    char *args_all[] = {"stress-ng", "--cpu", cworker, "--hdd", cworker,  "--vm",cworker, "--timeout", ctimeout, "--metrics", NULL};
     
    // change scheduler
    change_scheduler(0, SCHED_OTHER);
    
    switch(test_type&0b00001111){
        case 0b0001 : ret=execvp(args_cpu[0], args_cpu); break;
        case 0b0010 : ret=execvp(args_hdd[0], args_hdd); break;
        case 0b0011 : ret=execvp(args_cpu_hdd[0], args_cpu_hdd); break;
        case 0b0100 : ret=execvp(args_vm[0], args_vm); break;
        case 0b0111 : ret=execvp(args_all[0], args_all); break;
        default : exit(1); break; //ret=execvp(args_default[0], args_default); break;
    } 
    // If this is reached execvp failed.
    close(fd);
    perror("execvp");
    exit(EXIT_FAILURE);
}