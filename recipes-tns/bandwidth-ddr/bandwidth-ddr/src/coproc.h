#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
//#include <sys/ioctl.h>
//#include <termios.h>
#include <unistd.h> 

#define MAX_BUF 80

int copro_isFwRunning(void);
int copro_stopFw(void);
int copro_startFw(void);
 
int copro_getFwPath(char* pathStr);
int copro_setFwPath(char* pathStr);

int copro_getFwName(char* pathStr); 
int copro_setFwName(char* nameStr);

void copro_Init(char* firmwareName);