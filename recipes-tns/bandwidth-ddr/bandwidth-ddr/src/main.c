#include "main.h"
#include "serial.h"
#include "coproc.h"
#include "SDB_rpmsg.h"
//#include "sched-config.h"

#define BUFFER_SIZE 512

struct timeval tval_before, tval_after, tval_result;

int main() {

    // change scheduler
    change_scheduler(10, SCHED_RR);
    
    char tty_buf[BUFFER_SIZE]={'\0'};

printf("\n---------- Init firmware ----------\n");
    char firmware[40]="fw-bandwidth-DDR_CM4.elf";
    copro_stopFw();
    copro_Init(firmware);

printf("\n------------ Init TTY -------------\n");	
	int serial_port0 = open("/dev/ttyRPMSG0", O_RDWR);
	int serial_port1 = open("/dev/ttyRPMSG1", O_RDWR);
	if(init_tty(&serial_port0, B115200)){return 1;}
	if(init_tty(&serial_port1, B115200)){return 1;}

printf("\n------------ Init DDR -------------\n");

    // send number of shared data buffers to firmware
	sprintf(tty_buf, "B%d\n", NB_BUF);
	write(serial_port1, tty_buf, strlen(tty_buf));

	printf("Opening fd\n");
    SDB_Init();
   
/*
printf("\n------------- Write --------------\n");  
    int bufferToWrite = 0;
    char bufferW[40];

    for(int i = 0; i<5; i++){
       
        memset(bufferW, '\0', sizeof(bufferW));
        sprintf(bufferW,"[A7][SDB%d] hello from A7 %d\n",bufferToWrite, i);

        SDB_Write(bufferToWrite, bufferW, sizeof(bufferW));

        printf("[A7] ioctl RPMSG_SDB_IOCTL_SEND_DATA : buffId=%d size=%d msg=%s\n", bufferToWrite, sizeof(bufferW), bufferW);
        
        sleep(1);
        
    }


printf("\n-------------- Read --------------\n");
	
    int bufferToRead = 1;
    char bufferR[40];
    for(int i = 0; i<5; i++){
            
        memset(bufferR, '\0', sizeof(bufferR));

        if(!SDB_Read(bufferToRead, bufferR, sizeof(bufferR), 5000) ){
            printf("[A7] ioctl RPMSG_SDB_IOCTL_GET_DATA : buffId=%d size=%d msg=%s\n", bufferToRead, sizeof(bufferR), bufferR);
        }
    } 
*/
/*
printf("\n-------------- Echo --------------\n");

    for(int i =0; i<10; i++){
        char msg[40];
        memset(msg, '\0', sizeof(msg));
        sprintf(msg, "echo from A7 to M4 - %d", i);
        SDB_Echo(1,0, msg, sizeof(msg), 5000);
        sleep(2);
    } 
*/ 

uint32_t block_size;
int bufferToWrite = 0, bufferToRead = 1;
char savefile[40]={'\0'};

/*
printf("\n------------ Bandwidth -----------\n");

const int nb_tests = 10;
int data_size = 1024;
block_size = data_size;
sprintf(savefile, "bandwidth-ddr.csv");
struct test test_list[nb_tests];
init_save_file(savefile);

int maxtest = 1024;
data_size = 1024;
int i = 0;
    
for(i = 1; i<=maxtest; i+=1){     
    block_size = data_size*i;
    SDB_bandwidth_test(bufferToRead, bufferToWrite, test_list, nb_tests, block_size, 0);
    print_result(test_list, nb_tests , block_size);
    save_result (test_list,savefile, nb_tests , block_size);
}
*/
printf("\n------------- Stress -------------\n");
block_size = 1024*512;
int test_duration = 600;
int time_between2stress = 60;
int stress_duration = 60;
uint8_t test_type;
uint8_t worker = 2;

test_type = 0b0001; // stress cpu
sprintf(savefile, "stresstest-ddr-%dko-%dmin-id%d-w%d.csv", block_size/1024, test_duration/60, test_type, worker);
SDB_stress_test(bufferToRead, bufferToWrite, savefile, block_size, test_duration, time_between2stress, stress_duration, test_type, worker, 1);

test_type = 0b0010; // stress hdd
sprintf(savefile, "stresstest-ddr-%dko-%dmin-id%d-w%d.csv", block_size/1024, test_duration/60, test_type, worker);
SDB_stress_test(bufferToRead, bufferToWrite, savefile, block_size, test_duration, time_between2stress, stress_duration, test_type, worker, 1);

test_type = 0b0100; // stress ram
sprintf(savefile, "stresstest-ddr-%dko-%dmin-id%d-w%d.csv", block_size/1024, test_duration/60, test_type, worker);
SDB_stress_test(bufferToRead, bufferToWrite, savefile, block_size, test_duration, time_between2stress, stress_duration, test_type, worker, 1);

test_type = 0b0111; // stress cpu hdd ram
sprintf(savefile, "stresstest-ddr-%dko-%dmin-id%d-w%d.csv", block_size/1024, test_duration/60, test_type, worker);
SDB_stress_test(bufferToRead, bufferToWrite, savefile, block_size, test_duration, time_between2stress, stress_duration, test_type, worker, 1);

printf("\n-------------- End --------------\n");

    SDB_DeInit();
	printf("[A7] Buffers unmapped\n");

	printf("[A7] virtuart closed\n");
	close(serial_port0); close(serial_port1);

    printf("[A7] Shutting down coproc\n");
    copro_stopFw();
};

