// C library headers
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

// Linux headers
#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
//#include <termios.h> // Contains POSIX terminal control definitions
#include <time.h>
#include <unistd.h> // write(), read(), close()
#include <sys/mman.h> // mmap() munmap() - map and unmap files/devices in memory
#include <sys/eventfd.h> // event notification
#include <sys/ioctl.h> // control device

#include <sys/time.h>
#include <sys/poll.h>
#include <error.h>

#include <assert.h>

