SUMMARY = ""
DESCRIPTION = ""

LICENSE= "CLOSED"

FILES:${PN} += " /home/root \ 
                 /lib \
                "

SRC_URI += "file://src/"
SRC_URI += "file://firmware/"
                     
INHIBIT_SYSROOT_STRIP = "1"

DESTDIR = "/home/root/dev/bandwidth-ddr"

DEPENDS = " rpmsg-sdb-mod" 

do_install () {
    install -d ${D}${DESTDIR}    
    for file in ${WORKDIR}/src/*;do
        install -m 755 "$file" ${D}${DESTDIR} 
    done

    install -d ${D}/lib/firmware
    install -d ${D}${DESTDIR}/firmware
    for file in ${WORKDIR}/firmware/*;do
        install -m 755 "$file" ${D}/lib/firmware/
        install -m 755 "$file" ${D}${DESTDIR}/firmware/
    done
}

PACKAGE_ARCH = "${MACHINE_ARCH}"