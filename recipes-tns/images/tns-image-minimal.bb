
DESCRIPTION ="minimal image for stm32mp157f-dk2 with ssh"

LICENSE = "MIT"

require recipes-core/images/core-image-minimal.bb

SECTION = "images"

# Images features 
IMAGE_INSTALL += " \
    openssh openssh-sftp-server \
    nano nmon \
    util-linux procps\
    e2fsprogs-resize2fs \
    "

COMPATIBLE_MACHINE = "stm32mp1"

# Disable vendorfs and userfs
ST_BOOTFS   = "1"
ST_VENDORFS = "0"
ST_USERFS   = "0"

# Acceptance of the EULA to have acces to all stm features
ACCEPT_EULA_stm32mp1 = "1"

# Define specific board reference to use
M4_BOARDS = "STM32MP157F-DK2"

# -------------------------------------------------------------------------- #
# -------------- Remove unused features from machine stm32mp1 -------------- #
# -------------------------------------------------------------------------- #

# Remove unused boot scheme
BOOTSCHEME_LABELS:remove += " optee"

# Remove unused boot device
BOOTDEVICE_LABELS:remove = " emmc nand-4-256 nor-sdcard"

# Remove unused devicetree
STM32MP_DT_FILES_DK:remove += " stm32mp157a-dk1 stm32mp157d-dk1 stm32mp157c-dk2"
STM32MP_DT_FILES_ED:remove += " stm32mp157c-ed1 stm32mp157f-ed1"
STM32MP_DT_FILES_EV:remove += " stm32mp157a-ev1 stm32mp157c-ev1 stm32mp157d-ev1 stm32mp157f-ev1"

# Feature for eval board
KERNEL_MODULE_AUTOLOAD:remove += " goodix"

# Remove unused devicetree for Linux A7 examples
LINUX_A7_EXAMPLES_DT:remove += " stm32mp157c-dk2-a7-examples stm32mp157f-dk2-a7-examples stm32mp157c-ev1-a7-examples stm32mp157f-ev1-a7-examples"

# Remove unused devicetree for M4 example
CUBE_M4_EXAMPLES_DT:remove += " stm32mp157c-dk2-m4-examples stm32mp157f-dk2-m4-examples stm32mp157c-ev1-m4-examples stm32mp157f-ev1-m4-examples"
