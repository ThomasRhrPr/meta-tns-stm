
DESCRIPTION ="Image with graphical interface weston"

LICENSE = "MIT"

SECTION = "images"

require tns-image-weston.bb  

# build tools
inherit cmake

# package management 
PACKAGE_CLASSES = "package_deb"
IMAGE_INSTALL += " dpkg apt xz"
EXTRA_IMAGE_FEATURES += " package-management"

# TnS features : firmwares, bandwidth & stress test
IMAGE_INSTALL += " scripts-dev cm4firmwares-examples \
                   bandwidth-tty \
                   bandwidth-ddr \
                   can-bus \
                   bluetooth-wifi \
                   "

# bluetooth & wifi + driver bcm43430
IMAGE_INSTALL += " linux-firmware-bcm43430"
IMAGE_INSTALL += " wpa-supplicant iw dhcp-client"

IMAGE_INSTALL += " linux-firmware-bluetooth-bcm4343 bluez5"
#MACHINE_ESSENTIAL_EXTRA_RRECOMMENDS += " kernel-modules"

# vscode ssh remote requirements            
IMAGE_INSTALL += " glibc libc6-dev libstdc++ python3 tar bash curl"

# build tools
IMAGE_INSTALL += " packagegroup-core-buildessential cmake"

# bus CAN
IMAGE_INSTALL += " can-utils iproute2"

# monitoring tools
IMAGE_INSTALL += " nmon devmem2 stress-ng rt-tests hwlatdetect"

# kernel-headers
#IMAGE_INSTALL += " kernel-devsrc"

# trace tools             
IMAGE_INSTALL += " trace-cmd kernelshark"

# rpmsg
CORE_IMAGE_EXTRA_INSTALL += "rpmsg-sdb-mod"

# debug
EXTRA_IMAGE_FEATURES += " tools-debug" 

# Image size 
#IMAGE_ROOTFS_SIZE = " 763904"
#IMAGE_ROOTFS_SIZE = "1048576"
IMAGE_ROOTFS_MAXSIZE = "2097152"
#IMAGE_OVERHEAD_FACTOR = "2"
#IMAGE_ROOTFS_EXTRA_SPACE = "2097152"
