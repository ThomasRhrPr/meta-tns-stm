
DESCRIPTION ="Image minimal for Real Time test "

LICENSE = "MIT"

SECTION = "images"

require tns-image-minimal.bb  

# monitoring tools
IMAGE_INSTALL += " nmon devmem2 stress-ng rt-tests hwlatdetect"

# rpmsg
CORE_IMAGE_EXTRA_INSTALL += "rpmsg-sdb-mod"


