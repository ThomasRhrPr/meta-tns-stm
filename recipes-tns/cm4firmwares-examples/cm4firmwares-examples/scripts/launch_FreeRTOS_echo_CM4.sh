#!/bin/sh
echo stop > /sys/class/remoteproc/remoteproc0/state

echo OpenAMP_FreeRTOS_echo_CM4.elf > /sys/class/remoteproc/remoteproc0/firmware

echo start > /sys/class/remoteproc/remoteproc0/state

stty -onlcr -echo -F /dev/ttyRPMSG0
cat /dev/ttyRPMSG0 &

cat /sys/kernel/debug/remoteproc/remoteproc0/trace0

echo "
You can use following command : 
    echo hello > /dev/ttyRPMSG0
"