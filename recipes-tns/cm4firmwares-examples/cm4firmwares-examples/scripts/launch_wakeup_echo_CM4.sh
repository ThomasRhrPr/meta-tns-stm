#!/bin/sh
echo stop > /sys/class/remoteproc/remoteproc0/state

echo OpenAMP_TTY_echo_wakeup_CM4.elf > /sys/class/remoteproc/remoteproc0/firmware

echo start > /sys/class/remoteproc/remoteproc0/state

stty -onlcr -echo -F /dev/ttyRPMSG0
cat /dev/ttyRPMSG0 &

cat /sys/kernel/debug/remoteproc/remoteproc0/trace0

echo "
Some messages are handled specifically by CM4:
        - \"*stop\"    : upon reception of this message, CM4 goes to CStop mode and only allow entering System Stop mode
        - \"*standby\" : upon reception of this message, CM4 goes to CStop mode and allow entering System Stop or Standby mode.
        - \"*delay\"   : CM4 sends a RPMsg message to CA7 after a 20 second delay. It is a Wakeup source for CA7

Show CM4 log:
        cat /sys/kernel/debug/remoteproc/remoteproc0/trace0

Move CA7 in CStop mode :
    -with systemd : 
        systemctl suspend
    -whitout systemd : 
        echo mem > /sys/power/state

Move CM4 in CRun mode :
    -using User push button (PA14)
    -using Exti line 62 (IPCC interrupt CPU2)  
        echo wakeup > /dev/ttyRPMSG0

Move CA7 in CRun mode :
    -using Wake up button which raises Exti line WKUP1 (PIN PA0)
    -using Exti line 61 (IPCC interrupt CPU1):
        -set Exti line 61 (IPCC interrupt CPU1) as source of wakeup :
            echo enabled > /sys/devices/platform/soc/4c001000.mailbox/power/wakeup
        -configure CM4 to wakeup CA7 after 20 seconds
            echo \"*delay\" >/dev/ttyRPMSG0

"