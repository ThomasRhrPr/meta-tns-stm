#!/bin/sh
echo stop > /sys/class/remoteproc/remoteproc0/state

echo OpenAMP_TTY_echo_CM4.elf > /sys/class/remoteproc/remoteproc0/firmware

echo start > /sys/class/remoteproc/remoteproc0/state

sleep 0.1
stty -onlcr -echo -F /dev/ttyRPMSG0
cat /dev/ttyRPMSG0 &

sleep 0.1
stty -onlcr -echo -F /dev/ttyRPMSG1
cat /dev/ttyRPMSG1 &

cat /sys/kernel/debug/remoteproc/remoteproc0/trace0

echo "
You can use following commands : 
    echo hello > /dev/ttyRPMSG0
    echo hello > /dev/ttyRPMSG1
"