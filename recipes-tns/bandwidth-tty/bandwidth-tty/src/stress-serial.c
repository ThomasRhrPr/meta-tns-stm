#include "stress-serial.h"
#include "bandwidth-serial.h"

uint16_t isStressed=0;

void handler_SIGCHLD(int sig) { 
    signal(sig, handler_SIGCHLD);
    isStressed=0;
}


/* make_stress_test() parameters :
- tty_data : VirtUart used to make bandwidth test
- tty_ack : VirtUart used to make acknowledgment
- filename : name of the save file
- block_size : data size 
- test_duration : duration of the test in seconds 
- time_btw2stress : time elapsed between two stress test 
- stress_duration_sec : duration of stress test in seconds
- test_type : 
    - 0b0001 : cpu stress
    - 0b0010 : hdd stress
    - 0b0100 : memory stress
    - 0b0111 : cpu, hdd and memory stress
    - others : cpu, hdd, memory, mmap, memcpy stress
- worker : number of workers for each tests
*/
void make_stress_test(int *tty_data,int *tty_ack,char *filename, uint16_t block_size, uint32_t test_duration_sec, uint32_t time_btw2stress_sec, uint32_t stress_duration_sec, uint8_t test_type, uint8_t worker, int debug){
	static struct timespec tglobal_start, tglobal_stop, tread_start, tread_stop, twrite_start, twrite_stop, runtime;

    float globaltime, writetime, readtime, bw_global, bw_read, bw_write;
    uint16_t bytesread, state, isValid;
    uint16_t number_stress_test=0;
    runtime.tv_sec=0; runtime.tv_nsec=0;

    char ack_buff[5]={'\0'};
    static char tx_buff[512] = {'\0'}; 
    static char rx_buff[512] = {'\0'};
	init_buffer((char *)tx_buff, block_size);
    pid_t pidf;
    FILE *fp; 
    char buffer[300]={0};

    if(time_btw2stress_sec > test_duration_sec){printf("%s(error): time btw2stress > test duration\n", __func__); return;}
    if(stress_duration_sec > test_duration_sec){printf("%s(error): stress duration > test duration\n", __func__);return;}
    if(time_btw2stress_sec < stress_duration_sec){printf("%s(error): time btw2stress < stress duration\n", __func__);return;}

    //int stress_test2do = test_duration_sec/(time_btw2stress_sec + stress_duration_sec);

    // Init save file 
    fp = fopen(filename, "w");
    if ( fp == NULL ) {
        fprintf( stderr, "Cannot open file for writing\n" );
        exit( -1 );
    }
    sprintf(buffer, "time(s),bytes read,isStressed,isValid,global(us),A7toM4(us),M4toA7(us),bw_global(ko/s),bw_A7toM4(ko/s),bw_M4toA7(ko/s)\n");
    fputs(buffer, fp);
    fclose(fp);

    clock_gettime(CLOCK_REALTIME, &tglobal_start);

    // start test 
    while(test_duration_sec > runtime.tv_sec)
    {    
        clock_gettime(CLOCK_REALTIME, &twrite_start);
        write(*tty_data, tx_buff, strlen(tx_buff));
        read(*tty_ack, ack_buff, 3);  
        clock_gettime(CLOCK_REALTIME, &twrite_stop);

        // from M4 to A7
		clock_gettime(CLOCK_REALTIME, &tread_start);
        write(*tty_ack, "ACK\n\0", strlen("ACK\n\0"));
        bytesread=read(*tty_data, rx_buff, block_size*sizeof(char));  

        clock_gettime(CLOCK_REALTIME, &tread_stop);

        // runtime calculation
        if (tread_stop.tv_nsec-tglobal_start.tv_nsec < 0) {
            runtime.tv_sec  = tread_stop.tv_sec - tglobal_start.tv_sec - 1;
            runtime.tv_nsec = tread_stop.tv_nsec - tglobal_start.tv_nsec + 1000000000;
        } else {
            runtime.tv_sec  = tread_stop.tv_sec - tglobal_start.tv_sec;
            runtime.tv_nsec = tread_stop.tv_nsec - tglobal_start.tv_nsec;
        }

        globaltime  = timespec_diff_ns (twrite_start, tread_stop);
        writetime  = timespec_diff_ns (twrite_start, twrite_stop);
        readtime  = timespec_diff_ns (tread_start, tread_stop);


        state = strcmp(tx_buff, rx_buff);

        if(state==0){
            bw_global= ((float)block_size*2) / globaltime;
            bw_read= block_size / readtime,
            bw_write= block_size / writetime;
            isValid=1;
        }else{
            globaltime=0; bw_read=0; bw_write=0; isValid=0;
        }

        fp = fopen(filename, "a");
        sprintf(buffer, "%ld.%ld,%d,%d,%d,%.04f,%.04f,%.04f,%.04f,%.04f,%.04f\n", \
            runtime.tv_sec, runtime.tv_nsec, bytesread, isStressed, isValid, \
            globaltime/1000, writetime/1000, readtime/1000, bw_global*1000000, bw_write*1000000, bw_read*1000000);
        fputs(buffer, fp);
        fclose(fp);

        if(debug){
            sprintf(buffer, "[id:%d][%ld.%9ld] isStressed=%d, isValid=%d, bw_global(ko/s)=%.04f, bw_A7toM4(ko/s)=%.04f, bw_M4toA7(ko/s)=%.04f\n", \
                test_type, runtime.tv_sec, runtime.tv_nsec, isStressed, isValid, bw_global*1000000, bw_write*1000000, bw_read*1000000);
            printf("%s",buffer);
        }

        // create child to run stress test 
        if( ((runtime.tv_sec) >= (time_btw2stress_sec + (time_btw2stress_sec + stress_duration_sec)*number_stress_test)) && (isStressed==0) ){
            //number_stress_test++;
            switch(pidf=fork()){
                case -1:	//fork error
                    printf("%s: fork error\n", __func__); exit(-1); break;
                case 0:	    // child
                    start_stress(test_type, worker, stress_duration_sec); break; // make stress test 
                default: 
                    signal(SIGCHLD,handler_SIGCHLD);  // listen for end of child
                    number_stress_test++; isStressed=1;
            }    
        }
	}
    kill(pidf, SIGKILL); // kill child
    wait(NULL); // wait for end of child
    clock_gettime(CLOCK_REALTIME, &tglobal_stop);
}

void start_stress(uint8_t test_type, uint8_t worker, uint32_t timeout ){
    char ctimeout[4] = {'\0'};
    char cworker[4] = {'\0'};
    int ret;

    int fd = open("tmp.log", O_WRONLY | O_APPEND | O_CREAT, 0644);
    if ( fd < 0 ) { fprintf( stderr, "Cannot open file for writing\n" ); exit( -1 );}
    
    dup2(fd, STDOUT_FILENO); // redirect output to fd file 
    dup2(fd,STDERR_FILENO); // redirect logs to fd file 
    
    sprintf(ctimeout, "%d", timeout);
    sprintf(cworker, "%d", worker);
    char *args_cpu[] = {"stress-ng", "--cpu", cworker, "--timeout", ctimeout, "--metrics", NULL};
    char *args_hdd[] = {"stress-ng", "--hdd", cworker, "--timeout", ctimeout, "--metrics", NULL};
    char *args_cpu_hdd[] = {"stress-ng", "--cpu",cworker, "--hdd", cworker, "--timeout", ctimeout, "--metrics", NULL};
    char *args_vm[] = {"stress-ng", "--vm", cworker, "--timeout", ctimeout, "--metrics", NULL};
    char *args_all[] = {"stress-ng", "--cpu", cworker, "--hdd", cworker,  "--vm",cworker, "--timeout", ctimeout, "--metrics", NULL}; 

    switch(test_type&0b00001111){
        case 0b0001 : ret=execvp(args_cpu[0], args_cpu); break;
        case 0b0010 : ret=execvp(args_hdd[0], args_hdd); break;
        case 0b0011 : ret=execvp(args_cpu_hdd[0], args_cpu_hdd); break;
        case 0b0100 : ret=execvp(args_vm[0], args_vm); break;
        case 0b0111 : ret=execvp(args_all[0], args_all); break;
        default : exit(1); break; //ret=execvp(args_default[0], args_default); break;
    } 
    // If this is reached execvp failed.
    close(fd);
    perror("execvp");
    exit(EXIT_FAILURE);

}