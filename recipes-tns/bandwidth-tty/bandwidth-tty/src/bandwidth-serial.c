#include "bandwidth-serial.h"

// Init data buffer to send during test 
void init_buffer(char *buffer, uint16_t buffer_size){
     time_t t;
    srand((unsigned) time(&t)); //Intializes random number generator
    for(int i=0; i<buffer_size; i++){
        buffer[i]=33+(rand()%94); // ASCII from "!"(33) to "~"(126) for avoid "void" and control char
    }
}

// Run bandwidth test
void make_test(int *tty_data,int *tty_ack,struct test *test_list, uint16_t nb_tests, uint16_t block_size, int debuglvl){
	static struct timespec tglobal_start, tread_start, tglobal_stop, twrite_stop;

    char ack_buff[5]={'\0'};
    static char tx_buff[512] = {'\0'}; 
    static char rx_buff[512] = {'\0'};
	init_buffer((char *)tx_buff, block_size);

    for(int i=0;i<nb_tests; i++){

        if(debuglvl>=2){
            printf("-- test %d -- \n",i);
            printf("snd :\n %s\n", tx_buff);
        }

        // from A7 to M4
        clock_gettime(CLOCK_REALTIME, &tglobal_start);
        
        write(*tty_data, tx_buff, strlen(tx_buff));
        read(*tty_ack, ack_buff, 3);  
        if(debuglvl>=3){
            printf("ack :\n %s\n", ack_buff);
        }
        clock_gettime(CLOCK_REALTIME, &twrite_stop);

        // from M4 to A7
		clock_gettime(CLOCK_REALTIME, &tread_start);
        write(*tty_ack, "ACK\n\0", strlen("ACK\n\0"));
        test_list[i].bytesread=read(*tty_data, rx_buff, block_size*sizeof(char));  

        clock_gettime(CLOCK_REALTIME, &tglobal_stop);
        
          //  usleep(5000);
        test_list[i].globaltime  = timespec_diff_ns (tglobal_start, tglobal_stop);
        test_list[i].writetime  = timespec_diff_ns (tglobal_start, twrite_stop);
        test_list[i].readtime  = timespec_diff_ns (tread_start, tglobal_stop);

        if(debuglvl>=2){
            printf("rcv :\n %s\n", rx_buff);
        }

        test_list[i].id = i;
        test_list[i].size = block_size;
        test_list[i].state = strcmp(tx_buff, rx_buff);
        
        if(debuglvl>=1){
            printf("-> test: id=%d, size=%do, bytes read=%d, time(rw)=%d.%dus, time(w)=%d.%dus, time(r)=%d.%dus, losses=%d \n", \
                test_list[i].id, test_list[i].size, test_list[i].bytesread, \
                test_list[i].globaltime/1000, test_list[i].globaltime%1000,  \
                test_list[i].writetime/1000, test_list[i].writetime%1000,  \
                test_list[i].readtime/1000, test_list[i].readtime%1000,  \
                test_list[i].state);
        }   
	}
    // print_result(test_list, nb_tests , block_size);
}

// Calc and print bandwidth of tests 
void print_result(struct test *test_list, int nb_tests, int block_size){

    float tmoyglobal = 0,tmoywrite = 0, tmoyread= 0;
    int isValid=0;
    float bandwidthglobal=0,bandwidthwrite=0,bandwidthread=0;
    for(int i=0; i<nb_tests; i++){

        if(test_list[i].state == 0){
            tmoyglobal+=test_list[i].globaltime;
            tmoywrite+=test_list[i].writetime;
            tmoyread+=test_list[i].readtime;
            isValid++;
        }
    }

    if(isValid){
        tmoyglobal/=nb_tests;
        tmoywrite/=nb_tests;
        tmoyread/=nb_tests;
        
        bandwidthglobal = (block_size*2) / tmoyglobal; // block_size*2
        bandwidthwrite= block_size / tmoywrite;
        bandwidthread= block_size / tmoyread;
    }

    printf("\nBlocks size: %d \
            \nValid tests: %d/%d - %.02f%% \
            \nAverage time -rw: %.04fus \
            \n             -w: %.04fus \
            \n             -r: %.04fus \
            \nBandwidth -rw: %.04f ko/s \
            \n          -w: %.04f ko/s \
            \n          -r: %.04f ko/s\n", \
            block_size, isValid, nb_tests, ((float)isValid/(float)nb_tests)*100.0, \
            tmoyglobal/1000, tmoywrite/1000, tmoyread/1000, \
            bandwidthglobal*1000000,  bandwidthwrite*1000000,  bandwidthread*1000000 \
        );
}

// Init save file
void init_save_file(char *filename){
    FILE *fp; 
    char buffer[200]={0};

    fp = fopen(filename, "w");
    if ( fp == NULL ) {
        fprintf( stderr, "Cannot open file for writing\n" );
        exit( -1 );
    }

    sprintf(buffer, "Blocks size,Valid tests,Number of tests,Succes(%%),Average time (rw)(us),Average time (A7toM4)(us),Average time (M4toA7)(us),Bandwidth(rw)(ko/s),Bandwidth(A7toM4)(ko/s),Bandwidth(M4toA7)(ko/s)\n");
    fputs(buffer, fp);

    fclose(fp);

}

// Save results in the given file (filename)
void save_result(struct test *test_list,char *filename, int nb_tests, int block_size){
    FILE *fp; 
    char buffer[300]={0};

    fp = fopen(filename, "a");
    if ( fp == NULL ) {
        fprintf( stderr, "Cannot open file for writing\n" );
        exit( -1 );
    }

   // sprintf(buffer, "Blocks size,Valid tests,Total of tests,Average time (rw)(us),Average time (w)(us),Average time (r)(us),Bandwidth (rw)(ko/s),Bandwidth (w)(ko/s),Bandwidth(r)(ko/s)\n");
   // fputs(buffer, fp);

    float tmoyglobal = 0,tmoywrite = 0, tmoyread= 0;
    int isValid=0;
    float bandwidthglobal=0,bandwidthwrite=0,bandwidthread=0;
    for(int i=0; i<nb_tests; i++){

        if(test_list[i].state == 0){
            tmoyglobal+=test_list[i].globaltime;
            tmoywrite+=test_list[i].writetime;
            tmoyread+=test_list[i].readtime;
            isValid++;
        }
    }

    if(isValid){
        tmoyglobal/=nb_tests;
        tmoywrite/=nb_tests;
        tmoyread/=nb_tests;
        
        bandwidthglobal = (block_size*2) / tmoyglobal;
        bandwidthwrite= block_size / tmoywrite;
        bandwidthread= block_size / tmoyread;
    }

    sprintf(buffer, "%d,%d,%d,%.04f,%.04f,%.04f,%.04f,%.04f,%.04f,%.04f\n", \
            block_size, isValid, nb_tests, ((float)isValid/(float)nb_tests)*100.0, \
            tmoyglobal/1000, tmoywrite/1000, tmoyread/1000, \
            bandwidthglobal*1000000,  bandwidthwrite*1000000,  bandwidthread*1000000 \
        );
    fputs(buffer, fp);

        fclose(fp);
}

// Difference between 2 struct timespec
long timespec_diff_ns (struct timespec t1, struct timespec t2){
    struct timespec diff;
    if (t2.tv_nsec-t1.tv_nsec < 0) {
        diff.tv_sec  = t2.tv_sec - t1.tv_sec - 1;
        diff.tv_nsec = t2.tv_nsec - t1.tv_nsec + 1000000000;
    } else {
        diff.tv_sec  = t2.tv_sec - t1.tv_sec;
        diff.tv_nsec = t2.tv_nsec - t1.tv_nsec;
    }

    return (diff.tv_sec * 1000000000.0 + diff.tv_nsec);
}
