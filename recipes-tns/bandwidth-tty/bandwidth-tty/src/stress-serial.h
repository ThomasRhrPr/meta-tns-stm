#include <errno.h> 
#include <fcntl.h> 
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h> // write(), read(), close()
#include <errno.h>

void make_stress_test(int *tty_data,int *tty_ack,char *filename, uint16_t block_size, uint32_t test_duration_sec, uint32_t time_btw2stress_sec, uint32_t stress_duration_sec, uint8_t test_type, uint8_t worker, int debug);
void start_stress(uint8_t test_type,uint8_t worker, uint32_t timeout );