#include "serial.h"
#include "coproc.h"
#include "bandwidth-serial.h"
#include "stress-serial.h"

int main() {

    const int nb_tests = 500;
    int block_size = 0;
    char savefile[60]="bandwidthtest-tty.csv";
    struct test test_list[nb_tests];
    
    /******************** Init Firmware ********************/ 
    char firmware[40]="fw-bandwidth-tty_CM4.elf";
    copro_Init(firmware);

    /******************** Init TTY ********************/ 
    printf("Opening TTY...\n");
	int serial_port0 = open("/dev/ttyRPMSG0", O_RDWR);
    int serial_port1 = open("/dev/ttyRPMSG1", O_RDWR);
    printf("Init TTY...\n");
	if(init_tty(&serial_port0)){return 1;}
    if(init_tty(&serial_port1)){return 1;}

    /******************** bandwidth test ********************/ 
    init_save_file(savefile);
    for(block_size=1; block_size<=512; block_size+=1){
        make_test(&serial_port0, &serial_port1, test_list, nb_tests, block_size, 0);
        print_result(test_list, nb_tests , block_size);
        save_result (test_list,savefile, nb_tests , block_size);
    }

    /******************** Stability test ********************/ 
    block_size=460;

    sprintf(savefile, "stabilitytest-tty-%do-%dmin-id%d.csv", block_size, 300/60, 0);
    make_stress_test(&serial_port0, &serial_port1, savefile, block_size, 300, 300, 0, 0, 0, 1);
   

    /******************** Stres test ********************/ 
    block_size=460;
    int test_duration = 300;
    int time_between2stress = 30;
    int stress_duration = 30;
    uint8_t test_type;
    uint8_t worker = 2;

    test_type = 0b0001; // stress cpu
    sprintf(savefile, "stresstest-tty-%do-%dmin-id%d-w%d.csv", block_size, test_duration/60, test_type, worker);
    make_stress_test(&serial_port0, &serial_port1, savefile, block_size, test_duration, time_between2stress, stress_duration, test_type, worker, 1);
    test_type = 0b0010; // stress hdd
    sprintf(savefile, "stresstest-tty-%do-%dmin-id%d-w%d.csv", block_size, test_duration/60, test_type, worker);
    make_stress_test(&serial_port0, &serial_port1, savefile, block_size, test_duration, time_between2stress, stress_duration, test_type, worker, 1);
    test_type = 0b0100; // stress ram
    sprintf(savefile, "stresstest-tty-%do-%dmin-id%d-w%d.csv", block_size, test_duration/60, test_type, worker);
    make_stress_test(&serial_port0, &serial_port1, savefile, block_size, test_duration, time_between2stress, stress_duration, test_type, worker, 1);
    test_type = 0b0111; // stress all
    sprintf(savefile, "stresstest-tty-%do-%dmin-id%d-w%d.csv", block_size, test_duration/60, test_type, worker);
    make_stress_test(&serial_port0, &serial_port1, savefile, block_size, test_duration, time_between2stress, stress_duration, test_type, worker, 1);

    
    /******************** DeInit ********************/ 
	close(serial_port0);
    close(serial_port1);
    copro_stopFw();
	return 0; 
};
