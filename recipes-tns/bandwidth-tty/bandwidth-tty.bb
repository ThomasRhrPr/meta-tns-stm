SUMMARY = ""
DESCRIPTION = ""

LICENSE= "CLOSED"

FILES:${PN} += " /lib \ 
                 /home/root \
                "

SRC_URI += "file://src/ \
            file://firmware/ \
            "
                     
INHIBIT_SYSROOT_STRIP = "1"

DESTDIR = "/home/root/dev/bandwidth-tty"

do_install () {
    install -d ${D}${DESTDIR}    
    for file in ${WORKDIR}/src/*;do
        install -m 755 "$file" ${D}${DESTDIR} 
    done

    install -d ${D}/lib/firmware
    install -d ${D}${DESTDIR}/firmware
    for file in ${WORKDIR}/firmware/*;do
        install -m 755 "$file" ${D}/lib/firmware/
        install -m 755 "$file" ${D}${DESTDIR}/firmware/
    done
}

PACKAGE_ARCH = "${MACHINE_ARCH}"