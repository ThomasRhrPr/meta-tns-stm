SUMMARY = "ELF tns firmwares"
DESCRIPTION = "It installs elf files and scripts to develop on CM4"

LICENSE= "CLOSED"

FILES:${PN} += " /lib \
                 /home/root \
                 /media \
                "

SRC_URI += "file://dev-scripts/ \
            file://dev-firmwares/ \
            "
                     
INHIBIT_SYSROOT_STRIP = "1"

do_install () {

    install -d ${D}/home/root/scripts
    for file in ${WORKDIR}/dev-scripts/*;do
        install -m 755 "$file" ${D}/home/root/scripts
    done

    install -d ${D}/lib/firmware
    for file in ${WORKDIR}/dev-firmwares/*;do
        install -m 755 "$file" ${D}/lib/firmware/
    done

    install -d ${D}/media/boot
}

PACKAGE_ARCH = "${MACHINE_ARCH}"