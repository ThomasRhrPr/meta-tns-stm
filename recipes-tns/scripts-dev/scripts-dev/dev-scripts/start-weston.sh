#!/bin/sh
#start weston over ssh for x11
if [ "${BASH_SOURCE[0]}" -ef "$0" ]
then
    me="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
    echo "You should source this script, please run : source ./$me"
    exit 1
fi

#set XDG_RUNTIME_DIR
if test -z "${XDG_RUNTIME_DIR}"; then
    export XDG_RUNTIME_DIR=/tmp/${UID}-runtime-dir
    if ! test -d "${XDG_RUNTIME_DIR}"; then
        mkdir "${XDG_RUNTIME_DIR}"
        chmod 0700 "${XDG_RUNTIME_DIR}"
    fi
fi

#Start weston
weston