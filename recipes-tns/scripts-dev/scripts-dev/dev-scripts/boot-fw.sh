#!/bin/sh

#elf=coproc_sync_CM4.elf
elf=fw-rtos_CM4.elf

MOUNTDIR=/media/boot

if [ ! -d $MOUNTDIR ]; then
    mkdir $MOUNTDIR
fi
mount /dev/mmcblk0p4 $MOUNTDIR
cp /lib/firmware/$elf $MOUNTDIR
rm $MOUNTDIR/rproc-m4-fw.elf
mv $MOUNTDIR/$elf $MOUNTDIR/rproc-m4-fw.elf
sync
umount $MOUNTDIR
