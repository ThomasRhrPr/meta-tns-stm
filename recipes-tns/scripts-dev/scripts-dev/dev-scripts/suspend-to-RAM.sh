#!/bin/sh

echo "opening ttyRPMSG1"
stty -onlcr -echo -F /dev/ttyRPMSG1
cat /dev/ttyRPMSG1 > ttyRPMSG1.log &

echo "set Exti line 61 (IPCC interrupt CPU1) as source of wakeup"
echo enabled > /sys/devices/platform/soc/4c001000.mailbox/power/wakeup
sleep 1

echo "send: *delay"
echo *delay > /dev/ttyRPMSG1

sleep 1

echo "shut down"
echo mem > /sys/power/state

