# boot-fw.Sh 
Change firmware launch during boot. The name of the firmware must be set in "elf" variable.

The firmware must be installed in the folder "/lib/firmware".

# CM4_log.sh
Show CM4 trace. it's only work if log symbols __LOG_TRACE_IO_ is set during the build of the firmware.

# launch-fw.sh
Change the actual firmware.

The name of the firmware must be set in "elf" variable.

The firmware must be installed in the folder "/lib/firmware".

# loaddriver.sh
Load drivers. The name of the firmware must be set in "driver" variable.

# open_ttyRPMSG.sh
Set /dev/ttyRPMSG0 and /dev/ttyRPMSG1

# setdate.sh
Set date of the Linux, datas must be entered in variables

# start-weston.sh
Start weston over SSH and set XDG_RUNTIE_DIR variable

# suspend-to-RAM.sh
Set /dev/ttyRPMSG1

Set IPCC as wake-up source for Linux

Send *delay command to firmware (only work with firmware fw-rtos.elf & fw-bandwidth-ddr.elf)

Suspend-to-RAM Linux
